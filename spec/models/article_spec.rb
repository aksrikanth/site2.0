require 'rails_helper'

RSpec.describe Article, '#children' do
  it 'should properly construct a comment tree' do
    article = create(:article_with_comments, comment_count: 2)
    top_comments = article.children
    expect(article.comments.size).to equal(32)
    expect(top_comments.size).to equal(2)
    article.comments.each do |c|
      expect(c.children.size).to equal(3 - c.level)
    end
  end
end
