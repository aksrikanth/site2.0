require 'rails_helper'

RSpec.describe User, '#authenticate' do
  before (:all) do
    @default_user = create(:user, :with_password)
  end

  it 'successfuly authenticates a user with the correct password' do
    result = @default_user.authenticate('validpassword')

    expect(result).to be true
  end

  it 'fails to authenticate a user with an incorrect password' do
    result = @default_user.authenticate('incorrect')

    expect(result).to be false
  end
end
