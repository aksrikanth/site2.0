FactoryBot.define do
  factory :hash_code do
    salt { '$2a$10$kTktO6pTMMkceyH48t04eu' }
    hashed_password { '$2a$10$kTktO6pTMMkceyH48t04euu.uILp3PyCl/ZDi33IlnhYrgbrtH6RW' }
  end

  factory :user do
    display_name { 'Full Name' }
    sequence(:email) { |n| "user#{n}@example.com" }
    salt { '$2a$10$kTktO6pTMMkceyH48t04eu' }
    hashed_password { '$2a$10$kTktO6pTMMkceyH48t04euu.uILp3PyCl/ZDi33IlnhYrgbrtH6RW' }
    trait :with_password do
      password { 'validpassword' }
    end
  end

  factory :comment do
    sequence(:tldr) { |n| "tl;dr for comment #{n}" }
    body { 'Lorem ipsum *dolor* ~~sit~~ _amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.' }
    level { 0 }
    article
    parent { nil }
    user

    factory :comment_with_comments do
      transient do
        comment_count { 3 }
      end

      after(:create) do |comment, evaluator|
        create_list(:comment_with_comments, evaluator.comment_count, parent: comment, article: comment.article, level: comment.level + 1, comment_count: evaluator.comment_count - 1)
      end
    end
  end

  factory :article do
    sequence(:title) { |n| "Article Title #{n}" }
    sequence(:tldr) { |n| "Tldr for article #{n}." }
    body { 'Lorem ipsum *dolor* ~~sit~~ _amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.' }
    user

    factory :article_with_comments do
      transient do
        comment_count { 5 }
      end

      after(:create) do |article, evaluator|
        create_list(:comment_with_comments, evaluator.comment_count, article: article)
      end
    end
  end
end
