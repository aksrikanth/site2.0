class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:edit, :update]

  def new
    @user = User.new
  end

  def create
    @user = User.create(user_create_params)
    if @user.save
      helpers.log_in @user
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = helpers.current_user
    if @user.update(user_update_params)
      redirect_to root_path
    else
      render 'edit'
    end
  end

  private

  def user_create_params
    params.require(:user).permit(:display_name, :email, :password, :new_password)
  end

  def user_update_params
    params.require(:user).permit(:id, :display_name, :email)
  end
end
