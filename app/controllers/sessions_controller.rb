class SessionsController < ApplicationController
  def new
  end

  def create
    params.require(:session).permit(:email, :password)
    u = User.find_by_email(params[:session][:email])
    if u.present? && u.authenticate(params[:session][:password])
      helpers.log_in u
      redirect_to root_url
    else
      render 'new'
    end
  end

  def delete
    helpers.log_out
    redirect_to root_url
  end
end
