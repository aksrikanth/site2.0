class ArticlesController < ApplicationController
  def new
  end

  def create
  end

  def show
    @article = Article.with_comments.find(params[:id])
    @title = @article.title
  end

  def edit
  end

  def index
    @articles = if params[:draft]
      Article.with_user.unpublished
    else
      Article.with_user.published
    end
  end
end
