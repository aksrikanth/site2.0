class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, prepend: true

  def authenticate_user!
    redirect_to login_path unless helpers.logged_in?
  end

  def ensure_logged_out!
    redirect_to root_path if helpers.logged_in?
  end

  def robots
    render plain: (Rails.env.production? ? "" : "User-agent: *\nDisallow: /")
  end

  def sitemap
    headers['Content-Type'] = 'application/xml'
    respond_to do |format|
      format.xml do
        @articles = Article.published
        render layout: false
      end
    end
  end
end
