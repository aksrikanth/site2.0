class CommentsController < ApplicationController
  def show
    @comment = Comment.with_comments.find(params[:id])
    @title = "Comment on '#{@comment.article.title}'"
  end
end
