document.addEventListener("turbolinks:load", function () {
  if (document.body.hasAttribute('data-nav-loaded')) {
    return;
  } else {
    document.body.setAttribute('data-nav-loaded', true);
  }
  displayed = false;
  topHeader = document.getElementById('top_header');
  sidebarButton = document.getElementById('sidebar_button');
  sidebarButton.addEventListener('click', toggleSidebar);
  document.body.addEventListener('click', hideSidebar);

  function toggleSidebar(e) {
    if (displayed) {
      hideSidebar();
    } else {
      showSidebar();
    }
    e.stopPropagation();
  };

  function showSidebar() {
    topHeader.classList.add('display-list');
    displayed = true;
  }

  function hideSidebar() {
    topHeader.classList.remove('display-list');
    displayed = false;
  }
});
