module CommentTree
  def children
    construct if @children.nil?
    @children
  end

  def add_child(c)
    @children ||= []
    @children.append c
  end

  private

  def construct
    @children ||= []
    comment_hash = {}
    comment_hash[id] = self
    comments.each do |c|
      comment_hash[c.id] = c
      if c.level == 0
        add_child(c)
      end
    end

    comments.each do |c|
      comment_hash[c.parent_id].add_child(c) unless c.level == 0
    end
  end
end
