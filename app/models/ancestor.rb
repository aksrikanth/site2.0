class Ancestor < ApplicationRecord
  belongs_to :comment, foreign_key: 'comment_id', class_name: 'Comment'
  belongs_to :ancestor, foreign_key: 'ancestor_id', class_name: 'Comment'
end
