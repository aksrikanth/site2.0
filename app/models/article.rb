class Article < ApplicationRecord
  include CommentTree

  belongs_to :user

  has_many :comments

  scope :with_user, -> { includes(:user) }
  scope :with_comments, -> { includes(:user, comments: :user) }
  scope :reverse_chron, -> { order(updated_at: :desc) }
  scope :published, -> { where.not(published_at: nil).reverse_chron }
  scope :unpublished, -> { where(published_at: nil).reverse_chron }
end
