class User < ApplicationRecord
  validates :display_name, :email, :salt, :hashed_password, presence: true

  validate :password_strength

  before_save :save_hash

  def authenticate(password)
    hash1 = BCrypt::Engine.hash_secret(password, self.salt)
    h = HashCode.find_by_hashed_password(hash1)
    result = if h
      hash2 = BCrypt::Engine.hash_secret(password, h.salt)
      hash2 == self.hashed_password
    else
      false
    end
    return result
  end

  def password=(password)
    @password = password
    self.salt = BCrypt::Engine.generate_salt if self.salt.nil?
    @hash_code = HashCode.new
    @hash_code.set_password(password, self.salt)
    self.hashed_password = BCrypt::Engine.hash_secret(@password, @hash_code.salt)
  end

  private

  def password_strength
    return true if @password.nil?
    if @password.length < 10
      errors.add(:password, 'Password is too short')
      return false
    end
  end

  def save_hash
    @hash_code.save if @hash_code.present?
  end
end
