class Comment < ApplicationRecord
  include CommentTree

  has_many :comment_ancestors, foreign_key: :comment_id, class_name: 'Ancestor'
  has_many :ancestors, through: :comment_ancestors, source: :ancestor

  has_many :ancestor_comments, foreign_key: :ancestor_id, class_name: 'Ancestor'
  has_many :successors, through: :ancestor_comments, source: :comment

  belongs_to :user

  belongs_to :parent, foreign_key: 'parent_id', class_name: 'Comment', optional: true
  belongs_to :article

  validates :article, :body, :user, presence: true

  scope :with_user, -> { includes(:user) }
  scope :with_comments, -> { includes(:user, :parent, :article, successors: :user) }

  def comments
    successors
  end
end
