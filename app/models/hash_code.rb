class HashCode < ApplicationRecord
  self.primary_key = 'hashed_password'

  def set_password(password, salt)
    self.hashed_password = BCrypt::Engine.hash_secret(password, salt)
    self.salt = BCrypt::Engine.generate_salt if self.salt.nil?
  end
end
