atom_feed :language => 'en-us', root_url => root_url do |feed|
  feed.title 'Srikanth Agaram'
  feed.updated @articles.first.updated_at
  feed.author do |author|
    author.name 'Srikanth Agaram'
    author.url root_url
  end
  feed.icon asset_path('favicon.png')
  feed.rights copyright_message

  @articles.each do |article|
    feed.entry(
      article,
      id: "article#{article.id}",
      url: article_url(article),
      updated: article.updated_at,
    ) do |entry|
      entry.title article.title
      entry.summary render(partial: 'articles/tldr', formats: [:html], locals: {article: article}), type: 'html'
      entry.link rel: 'alternate', href: article_url(article)
      entry.author do |author|
        author.name article.user.display_name
        author.url root_url
      end
    end
  end
end
