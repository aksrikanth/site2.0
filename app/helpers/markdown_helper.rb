require 'markdown_renderer'

module MarkdownHelper
  def article_markdown(text)
    init_markdown if @article_markdown.nil?
    @article_markdown.render(text).html_safe
  end

  def comment_markdown(text)
    init_markdown if @comment_markdown.nil?
    @comment_markdown.render(text).html_safe
  end

  private

	def init_markdown
    article_options = {
      link_attributes: {
        target: "_blank",
      },
      space_after_headers: true,
      with_toc_data: true,
    }

    article_extensions = {
      highlight: true,
      strikethrough: true,
      superscript: true,
      underline: true,
      fenced_code_blocks: true,
      no_intra_emphasis: true,
      tables: true,
      footnotes: true,
    }

    article_renderer = MarkdownRenderer.new(article_options)
    @article_markdown = Redcarpet::Markdown.new(article_renderer, article_extensions)

    comment_options = {
      filter_html: true,
      safe_links_only: true,
      no_styles: true,
      link_attributes: {
        rel: 'nofollow',
        target: "_blank",
      },
      space_after_headers: true,
    }

    comment_extensions = {
      autolink: false,
      superscript: true,
      no_intra_emphasis: true,
    }

    comment_renderer = Redcarpet::Render::HTML.new(comment_options)
    @comment_markdown = Redcarpet::Markdown.new(comment_renderer, comment_extensions)
	end
end
