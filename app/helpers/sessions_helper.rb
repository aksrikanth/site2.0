module SessionsHelper
  def log_in(user)
    session[:user_id] = user.id
    session[:maybe_user_id] = user.id
  end

  def log_out
    session.delete :user_id
  end

  def current_user
    return nil if session[:user_id].nil?
    @current_user ||= User.find_by_id(session[:user_id])
  end

  def maybe_user
    return nil if session[:maybe_user_id].nil?
    @maybe_user ||= current_user
    @maybe_user ||= User.find_by_id(session[:maybe_user_id])
  end

  def logged_in?
    current_user.present?
  end
end
