Rails.application.routes.draw do
  resources :articles, only: [:new, :create, :show, :edit, :update, :index]
  resources :drafts, controller: 'articles', only: [:index], draft: true
  resources :comments, only: [:new, :create, :show, :edit, :update]
  resource :user, only: [:create, :update]
  resource :session, only: [:create]
  match '/code', to: 'pages#code', as: 'code', via: :get
  match '/publications', to: 'pages#publications', as: 'publications', via: :get
  match '/signup', to: 'users#new', as: 'signup', via: :get
  match '/profile', to: 'users#edit', as: 'profile', via: :get
  match '/login', to: 'sessions#new', as: 'login', via: :get
  match '/logout', to: 'sessions#delete', as: 'logout', via: :delete
  match '/robots.txt', to: 'application#robots', via: :get
  match '/sitemap.xml', to: 'application#sitemap', via: :get
  root to: 'articles#index', type: :article
end
