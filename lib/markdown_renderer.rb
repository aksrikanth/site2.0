require 'redcarpet'
require 'rouge'
require 'rouge/plugins/redcarpet'

class MarkdownRenderer < Redcarpet::Render::HTML
  include Rouge::Plugins::Redcarpet

  def self.start_section_text
    '<div class="section"><div class="text">'
  end

  def self.end_section_text
    '</div></div>'
  end

  def self.end_text
    '</div>'
  end

  def self.end_section
    '</div>'
  end

  def rouge_formatter(lexer)
    Rouge::Formatters::HTMLPygments.new(Rouge::Formatters::HTML.new)
  end

  def doc_footer
    return self.class.end_section_text if @started
    nil
  end

  def paragraph(text)
    return nil if text.strip.empty?
    return figure(text) if text.strip =~ /\A<img [^>]*>\z/
    p = "<p>#{text}</p>"
    if @started
      p
    else
      @started = true;
      self.class.start_section_text + p
    end
  end

  def figure(text)
    return nil if text.strip.empty?
    f = "<figure class=\"figure\">#{text}</figure>"
    if @started
      @started = false
      self.class.end_text + f + self.class.end_section
    else
      f
    end
  end

  def header(text, header_level)
    return nil if text.strip.empty?
    header_level += 1
    header = "#{@started ? self.class.end_section_text : ''}<h#{header_level}>#{text}</h#{header_level}>"
    @started = false
    header
  end

  def block_code(code, language)
    code = super(code, language)
    @started = false
    code.nil? ? nil : self.class.end_text + code + self.class.end_section
  end
end
