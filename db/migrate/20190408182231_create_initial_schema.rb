class CreateInitialSchema < ActiveRecord::Migration[5.2]
  def change
    create_table :articles do |t|
      t.string :title, null: false
      t.string :tldr, null: false
      t.text :body, null: false
      t.belongs_to :user
      t.datetime :published_at

      t.timestamps
    end

    add_index :articles, :published_at, where: 'published_at IS NOT NULL'

    create_table :comments do |t|
      t.string :tldr
      t.text :body, null: false
      t.integer :level
      t.belongs_to :article, index: true
      t.integer :parent_id
      t.belongs_to :user

      t.timestamps
    end

    create_table :ancestors do |t|
      t.integer :comment_id
      t.integer :ancestor_id
    end

    add_index :ancestors, :ancestor_id

    create_table :users  do |t|
      t.string :email, unique: true, null: false
      t.string :display_name, unique: true, null: false
      t.string :salt, null: false
      t.string :hashed_password, null: false

      t.timestamps
    end

    execute 'CREATE TABLE hash_codes ("hashed_password" varchar PRIMARY KEY, "salt" varchar NOT NULL)'

  end
end
