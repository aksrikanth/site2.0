# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_08_182231) do

  create_table "ancestors", force: :cascade do |t|
    t.integer "comment_id"
    t.integer "ancestor_id"
    t.index ["ancestor_id"], name: "index_ancestors_on_ancestor_id"
  end

  create_table "articles", force: :cascade do |t|
    t.string "title", null: false
    t.string "tldr", null: false
    t.text "body", null: false
    t.integer "user_id"
    t.datetime "published_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["published_at"], name: "index_articles_on_published_at", where: "published_at IS NOT NULL"
    t.index ["user_id"], name: "index_articles_on_user_id"
  end

  create_table "comments", force: :cascade do |t|
    t.string "tldr"
    t.text "body", null: false
    t.integer "level"
    t.integer "article_id"
    t.integer "parent_id"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["article_id"], name: "index_comments_on_article_id"
    t.index ["user_id"], name: "index_comments_on_user_id"
  end

  create_table "hash_codes", primary_key: "hashed_password", id: :string, force: :cascade do |t|
    t.string "salt", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "display_name", null: false
    t.string "salt", null: false
    t.string "hashed_password", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
