require 'bcrypt'
require 'securerandom'

User.delete_all
HashCode.delete_all
Article.delete_all
Comment.delete_all
Ancestor.delete_all

def parent_id(i)
  p = (i - 1)/2
  p == 0 ? i : p
end

def root_id(i)
  while parent_id(i) != i
    i = parent_id(i)
  end
  i
end

tldr = <<-'END'
Lorem ipsum *dolor* ~~sit~~ _amet_, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
END

article_body = <<-'END'
# An Article Title

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

## A Subtitle

This is a *Test*.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
[Example](http://example.com/)

```ruby
require("units")

print "You have: "
while from = gets
  from.chomp!

  print "You want: "
  to = gets.chomp

  begin
    result = Units.ratio(from.clone, to.clone)
    print from, " = ", result, " ", to, "\n"
  rescue Exception
    # Stupidly enough, Exception isn't the default; StandardError is, which is
    # a subclass of Exception.
    print "Failed: #{$!}\n"
  end

  print "You have: "
end
```

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

![Image](//www.cricinfo.com/db/PICTURES/CMS/100500/100551.jpg)
END

comment_body = <<-'END'
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

This is a *Test*.

[Example](http://example.com/)

    def test()
      puts "hello world                                                                                                   a"
    end
END

3.times do |i|
  password = "password#{i}"
  salt1 = BCrypt::Engine.generate_salt
  salt2 = BCrypt::Engine.generate_salt
  hash1 = BCrypt::Engine.hash_secret(password, salt1)
  hash2 = BCrypt::Engine.hash_secret(password, salt2)
  puts "hash1: #{hash1}"
  puts "hash2: #{hash2}"

  User.create!(
    id: i,
    email: "user#{i}@example.com",
    display_name: "user #{i}",
    salt: salt1,
    hashed_password: hash2
  )
  puts "user #{i}"

  HashCode.create!(
    hashed_password: hash1,
    salt: salt2
  )
  puts "hashcode #{i}"
end

def add_comment(tldr, message_body, article_id, parent_id, depth, limit)
  params = {
    tldr: tldr,
    body: message_body,
    level: depth,
    article_id: article_id,
    parent_id: (depth == 0 ? nil : parent_id),
    user_id: SecureRandom.rand(3),
  }
  c = Comment.create!(params)
  unless depth == 0
    Ancestor.create!(
      comment_id: c.id,
      ancestor_id: parent_id
    )
    c.parent.ancestors.each do |a|
      Ancestor.create!(
        comment_id: c.id,
        ancestor_id: a.id,
      )
    end
  end
  puts "comment #{c.id}"
  depth += 1
  unless depth == limit
    2.times do
      add_comment(tldr, message_body, article_id, c.id, depth, limit)
    end
  end
end

5.times do |i|
  params = {
    id: i,
    title: "This is a Title #{i}",
    tldr: tldr,
    body: article_body,
    user_id: SecureRandom.rand(3),
    published_at: Time.now,
  }
  puts params.inspect
  Article.create!(params)
  puts "article #{i}"
  2.times do |j|
    add_comment(tldr, comment_body, i, 0,  0, 5)
  end
end
